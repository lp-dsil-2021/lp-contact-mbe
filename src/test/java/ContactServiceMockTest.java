import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.junit.Test;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ContactServiceMockTest {

    @Mock
    private IContactDAO contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Captor
    ArgumentCaptor<Contact> contactCaptor;

    @Test(expected = ContactException.class)
    public void shouldFailDuplicateEntry() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(true);
        contactService.creerContact("Thierry");
    }

    @Test()
    public void shouldPass() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Benjamin")).thenReturn(false);
        contactService.creerContact("Benjamin");
    }

    @Test()
    public void shouldPassCaptor() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Thierry")).thenReturn(false);
        contactService.creerContact("Thierry");

        //V&eacute;rification de l'appel de la m&eacute;thode add de ContactDAO
        Mockito.verify(contactDao).add(contactCaptor.capture());
        //Je souhaite voir le contenu de l'argument
        Contact value = contactCaptor.getValue();
        Assertions.assertEquals("Thierry", value.getName());
    }

    @Test()
    public void shouldPassWithSpaceInTheName() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Daniel")).thenReturn(false);
        contactService.creerContact(" Daniel ");
    }
    @Test()
    public void shouldPassWithSurname() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Benjamin","Lambert")).thenReturn(false);
        contactService.creerContact("Benjamin","Lambert");
    }

    @Test(expected = ContactException.class)
    public void shouldFailDuplicateEntryWithSurname() throws ContactException {
        //Définition du mock
        Mockito.when(contactDao.isContactExist("Benjamin","Lambert")).thenReturn(true);
        contactService.creerContact("Benjamin","Lambert");
    }
}
