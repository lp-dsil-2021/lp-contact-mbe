public class Contact {

    private String nom;
    private String prenom;
    
    public Contact(String nom) {
        this.nom = nom;
    }
    public Contact(String nom, String prenom) {
        this.prenom = prenom;
    }

    public String getName() {
        return nom;
    }
    public String getSurname() {return prenom;}


}
